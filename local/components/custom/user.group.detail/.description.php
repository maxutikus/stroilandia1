<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("USERS_DESC_DEETAIL"),
	"DESCRIPTION" => GetMessage("USERS_DESC_DETAIL_DESC"),
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "custom",
		"NAME" => GetMessage("U_DESC_CUSTOM"),
	),
);

?>