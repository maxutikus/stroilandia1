<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentParameters = array(
	"PARAMETERS"  =>  array(
		"USER_GROUP_ID"  =>  Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("USERS_GROUPS_DETAIL_ID_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
	),
);
?>
