<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

global $USER;
global $APPLICATION;

use \Bitrix\Main\Loader,
	\Bitrix\Main;

if ($arParams['USER_GROUP_ID'])
	$arParams['USER_GROUP_ID'] = intval($arParams['USER_GROUP_ID']);


$arResult['USER_GROUP_INFO'] = [];

if (!$arParams['USER_GROUP_ID']) {
	ShowError(GetMessage("NO_ID_USER_GROUP"));
	return;
}

$rsGroups = Main\GroupTable::getList(array(
	'select'  => array('ID', 'NAME', 'DESCRIPTION'),
	'filter'  => array('ID' => $arParams['USER_GROUP_ID'])
));

if ($arGroup = $rsGroups->fetch()) {
	$arResult['USER_GROUP_INFO'] = $arGroup;
}

$this->includeComponentTemplate();

?>