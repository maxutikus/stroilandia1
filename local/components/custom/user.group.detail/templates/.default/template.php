<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if (!empty($arResult['USER_GROUP_INFO'])) {?>
	<table>
		<thead>
			<tr>
				<?foreach ($arResult['USER_GROUP_INFO'] as $fieldName => $info) {?>
					<th><?echo $fieldName?></th>
				<?}?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<?foreach ($arResult['USER_GROUP_INFO'] as $fieldName => $info) {?>
					<td><?echo $info?></td>
				<?}?>
			</tr>
		</tbody>
	</table>
<?}?>

