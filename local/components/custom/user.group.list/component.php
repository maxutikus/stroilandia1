<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

global $USER;
global $APPLICATION;

use \Bitrix\Main\Loader,
	\Bitrix\Main;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arResult['USERS_GROUPS'] = [];
if($this->startResultCache(false))
{
	$rsGroups = Main\GroupTable::getList(array(
		'select'  => array('ID', 'NAME', 'DESCRIPTION'),
		'filter'  => array()
	));

	while ($arGroup = $rsGroups->fetch()) {
		$arResult['USERS_GROUPS'][] = $arGroup;
	}

	$this->includeComponentTemplate();
}

if ($arParams['LIST_TITLE'])
{
	$APPLICATION->SetTitle($arParams['LIST_TITLE'], true);
}
?>