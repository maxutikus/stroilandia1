<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if (!empty($arResult['USERS_GROUPS'])) {?>
	<table>
		<thead>
			<tr>
				<th><?echo GetMessage('USERS_GROUPS_COLUMN_ID_TEXT')?></th>
				<th><?echo GetMessage('USERS_GROUPS_COLUMN_NAME_TEXT')?></th>
				<th><?echo GetMessage('USERS_GROUPS_COLUMN_DESCRIPTION_TEXT')?></th>
			</tr>
		</thead>
		<tbody>
		<?foreach ($arResult['USERS_GROUPS'] as $arGroup) {?>
			<tr>
				<td><?echo $arGroup['ID']?></td>
				<td><?echo $arGroup['NAME']?></td>
				<td><?echo $arGroup['DESCRIPTION']?></td>
			</tr>
		<?}?>
		</tbody>
	</table>
<?}?>
