<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("С_USERS_DESC_LINE"),
	"DESCRIPTION" => GetMessage("С_USERS_DESC_LINE_DESC"),
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "custom",
		"NAME" => GetMessage("U_DESC_CUSTOM"),
	),
);

?>