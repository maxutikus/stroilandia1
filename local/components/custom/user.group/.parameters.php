<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentParameters = array(
	"PARAMETERS"  =>  array(
		"LIST_TITLE"  =>  Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("USERS_GROUPS_LIST_TITLE_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CACHE_TIME"  =>  array(
			"NAME" => GetMessage("USERS_GROUPS_LIST_CACHE_TIME_NAME"),
			"DEFAULT"=>3600
		),
	),
);
?>
