<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$arDefaultUrlTemplates404 = [
	'list'    => '',
	'detail' => '#ELEMENT_ID#/',
];

$arDefaultVariableAliases404 = [];
$arDefaultVariableAliases    = [];
$arComponentVariables        = ['LIST_TITLE'];
$SEF_FOLDER                  = '/groups/';
$arUrlTemplates              = [];

$arVariables = [];

$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
	$arDefaultUrlTemplates404,
	[]
);

$arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
	$arDefaultVariableAliases404,
	[]
);

$componentPage = CComponentEngine::ParseComponentPath(
	$SEF_FOLDER,
	$arUrlTemplates,
	$arVariables
);

if (strlen($componentPage) <= 0) {
	$componentPage = 'list';
}

CComponentEngine::InitComponentVariables(
	$componentPage,
	$arComponentVariables,
	$arVariableAliases,
	$arVariables);

$arResult = [
	'FOLDER'        => $SEF_FOLDER,
	'URL_TEMPLATES' => $arUrlTemplates,
	'VARIABLES'     => $arVariables,
	'ALIASES'       => $arVariableAliases,
];

$this->IncludeComponentTemplate($componentPage);

?>