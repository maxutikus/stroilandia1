<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$APPLICATION->IncludeComponent(
	"custom:user.group.list",
	".default",
	Array(
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"COMPONENT_TEMPLATE" => ".default",
		"LIST_TITLE" => $arParams["LIST_TITLE"],
		"FOLDER" => $arResult["FOLDER"]
	), $component
);?>